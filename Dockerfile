FROM python:3.8-slim

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

ENV FLASK_APP main:app

CMD [ "gunicorn", "-b", "0.0.0.0", "main:app" ]
