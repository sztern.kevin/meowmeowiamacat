# Installing

pip install -r requirements.txt

# Running the app locally

FLASK_APP=main:app flask run

# Running in prod

See Dockerfile
